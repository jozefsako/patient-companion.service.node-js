const graphql = require("graphql");
const Tip = require("../models/Tip");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
} = graphql;

const TipType = new GraphQLObjectType({
  name: "Tip",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    imgUrl: { type: GraphQLString },
    externalLink: { type: GraphQLString },
    isTipOfTheDay: { type: graphql.GraphQLBoolean },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    tip: {
      type: TipType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Tip.findById(args.id);
      },
    },
    tips: {
      type: new GraphQLList(TipType),
      resolve(parent, args) {
        return Tip.find({});
      },
    },
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    addTip: {
      type: TipType,
      args: {
        //GraphQLNonNull make these fields required
        name: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: new GraphQLNonNull(GraphQLString) },
        imgUrl: { type: new GraphQLNonNull(GraphQLString) },
        externalLink: { type: new GraphQLNonNull(GraphQLString) },
        isTipOfTheDay: { type: graphql.GraphQLBoolean },
      },
      resolve(parent, args) {
        let tip = new Tip({
          name: args.name,
          description: args.description,
          imgUrl: args.imgUrl,
          externalLink: args.externalLink,
          isTipOfTheDay: args.isTipOfTheDay,
        });
        return tip.save();
      },
    },
    flagTipOfTheDay: {
      type: TipType,
      args: {
        tipId: { type: new GraphQLNonNull(GraphQLID) },
      },
      async resolve(parent, args) {
        try {
          const { tipId } = args;

          // Unflag any tips that are currently flagged as "Tip of the Day"
          const previouslyFlaggedTip = await Tip.findOneAndUpdate(
            { isTipOfTheDay: true },
            { isTipOfTheDay: false },
            { new: true }
          );

          // Flag the specified tip as "Tip of the Day"
          const newTip = await Tip.findByIdAndUpdate(
            tipId,
            { isTipOfTheDay: true },
            { new: true }
          );

          return newTip;
        } catch (error) {
          console.error(error);
        }
      },
    },
    removeTip: {
      type: TipType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve(parent, args) {
        return Tip.findByIdAndRemove(args.id);
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});

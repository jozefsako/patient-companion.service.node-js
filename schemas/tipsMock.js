const mockedUrl = "https://picsum.photos/400/250";

var fakeTipsDatabase = [
  {
    id: 1,
    name: "Swallowing",
    description:
      "If you struggle with swallowing, eat soft foods wherever possible. You can check out some of our soft food recipes for ideas here.",
    imgUrl: mockedUrl,
    externalLink: mockedUrl,
  },
  {
    id: 2,
    name: "Keep an eye open for the weater",
    description:
      "Temperature can have an effect on muscle function and extremes of temperature can exacerbate symptoms.",
    imgUrl: mockedUrl,
    externalLink: mockedUrl,
  },
  {
    id: 3,
    name: "Swallowing",
    description:
      "If you struggle with swallowing, eat soft foods wherever possible. You can check out some of our soft food recipes for ideas here.",
    imgUrl: mockedUrl,
    externalLink: mockedUrl,
  },
];

module.exports = fakeTipsDatabase;

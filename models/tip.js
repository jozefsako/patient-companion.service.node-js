const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const tipSchema = new Schema({
  name: String,
  description: String,
  imgUrl: String,
  externalLink: String,
  isTipOfTheDay: Boolean,
});

module.exports = mongoose.model("Tip", tipSchema);

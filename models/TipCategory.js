const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TipCategorySchema = new Schema({
  name: { type: String, required: true },
  tips: [{ type: Schema.Types.ObjectId, ref: "Tip" }],
});

module.exports = mongoose.model("TipCategory", TipCategorySchema);

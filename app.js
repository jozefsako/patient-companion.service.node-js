const express = require("express");
const mongoose = require("mongoose");

const { graphqlHTTP } = require('express-graphql');
require("dotenv/config");

const app = express();
const port = process.env.PORT || 3010;

const tipSchema = require("./schemas/tipSchema");

app.use(
  "/graphql",
  graphqlHTTP({
    schema: tipSchema,
    graphiql: true,
  })
);

// Connect to DB
mongoose.set("strictQuery", false);
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }, () => {
  console.log("[DB]: connected");
});

console.log(`[App]: is listening on the port: ${port}`);
app.listen(port);